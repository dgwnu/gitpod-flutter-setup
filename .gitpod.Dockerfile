FROM gitpod/workspace-base

USER gitpod

RUN mkdir downloads
RUN curl https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.7.0-stable.tar.xz --output downloads/flutter.tar.xz
RUN tar xf downloads/flutter.tar.xz
RUN echo 'export PATH="$PATH:~/flutter/bin"' >> ~/.bashrc
